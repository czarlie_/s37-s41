const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseControllers')
const auth = require('../auth')

// create a course route
router.post('/', auth.verify, (req, res) => {
  // data.course.name
  // data.isAdmin
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  }

  courseController
    .addCourse(data)
    .then((resultFromCourseController) => res.send(resultFromCourseController))
})

// retreiving all the courses route
router.get('/all', (req, res) => {
  courseController
    .getAllCourses()
    .then((resultFromCourseController) => res.send(resultFromCourseController))
})

// retreive all 'active' courses route
router.get('/', (req, res) => {
  courseController
    .getAllActiveCourses()
    .then((resultFromCourseController) => res.send(resultFromCourseController))
})

// update course
// JWT verification is needed for this route to ensure that the user is logged in before updating a course
router.put('/:courseId', auth.verify, (req, res) => {
  courseController
    .updateCourse(req.params, req.body)
    .then((resultFromCourseController) => res.send(resultFromCourseController))
})

// archive course
// v2 = req.body
router.patch('/:courseId/archive', auth.verify, (req, res) => {
  courseController
    .archiveCourses(req.param, req.body)
    .then((resultFromCourseController) => res.send(resultFromCourseController))
})

// unarchive course
router.patch('/:courseId/unarchive', auth.verify, (req, res) => {
  courseController
    .unarchiveCourse(req.params.courseId, req.body)
    .then((resultFromCourseController) => res.send(resultFromCourseController))
})

// allows us to export the 'router' object that will be accessed in our index.js
module.exports = router
