const express = require('express')
const router = express.Router()
const userController = require('../controllers/userControllers')
const auth = require('../auth')

// Route for checking if the user's email already exists in the bookingDatabase
// Invoke the checkEmailExists function from the controller file later to communicate with our bookingDatabase
// Passes the 'body' property of our 'request' object to the corresponding controller function
router.post('/checkEmail', (request, response) => {
  userController
    .checkEmailExists(request.body)
    .then((resultFromUserController) => response.send(resultFromUserController))
})

// Register user
router.post('/register', (request, response) => {
  userController
    .registerUser(request.body)
    .then((resultFromUserController) => response.send(resultFromUserController))
})

// Authenticate user
router.post('/login', (request, response) => {
  userController
    .loginUser(request.body)
    .then((resultFromUserController) => response.send(resultFromUserController))
})

// Get user details with empty password
// router.post('/details', (request, response) => {
//   userController
//     .getProfile(request.body)
//     .then((resultFromUserController) => response.send(resultFromUserController))
// })

router.get('/details', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization)

  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController))
})

// Allows us to export the router object that will be accessed
module.exports = router
