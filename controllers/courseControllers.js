const Course = require('../models/Course')

// my original answer:
// module.exports.addCourse = (reqBody) => {
//   let newCourse = new Course({
//     name: reqBody.name,
//     description: reqBody.description,
//     price: reqBody.price
//   })
//   return newCourse.save().then((course, error) => (error ? false : true))
// }

// add course
module.exports.addCourse = (data) => {
  if (data.isAdmin) {
    let newCourse = new Course({
      name: data.course.name,
      description: data.course.description,
      price: data.course.price,
    })
    return newCourse.save().then((course, error) => (error ? false : true))
  } else return false
}

// retreive all courses
/*
  Steps:
  1) Retrieve all the courses from the database
 */
module.exports.getAllCourses = () => {
  return Course.find({}).then((courseResult) => courseResult)
}

// retrieve all 'active' courses
/*
  Steps:
  1) Retrieve all the courses from the bookingDatabase with the property 'isActive' with the value: 'true'
*/
module.exports.getAllActiveCourses = () => {
  return Course.find({ isActive: true }).then((courseResult) => courseResult)
}

// update course
/*
  Steps:
  1) Create a variable 'updateCourse' which will contain information retrieved from the 'reqBody',
  2) Find and update the course using the courseId retrieved from the 'reqParams' property and 'updatedCourse' variable which contains the information from the 'reqBody'
*/
module.exports.updateCourse = (reqParams, reqBody) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  }

  // findByIdAndUpdate(document ID, updatesToBeApplied)
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
    (updatedCourse, error) => (error ? false : true)
  )
}

// archive course
//* in managing database, it's a common practice to 'soft delete' our records and what we would implement in the 'delete' operation of our application
// v2
module.exports.archiveCourses = (reqParams, reqBody) => {
  let updatedResult = { isActive: reqBody.isActive }

  return Course.findByIdAndUpdate(reqParams.courseId, updatedResult).then(
    (updatedIsActive, error) => (error ? false : true)
  )
}

// v1
// module.exports.archiveCourses = (reqParams) => {
//   Course.findById(reqParams.courseId).then((result, error) => {
//     console.log(result)
//     console.log(result.isActive)
//     result.isActive = 'komo'

//     return result
//       .save()
//       .then((updatedIsActive, error) => (error ? false : true))
//     // return newCourse.save().then((course, error) => (error ? false : true))
//   })
// }

module.exports.unarchiveCourse = (reqParams, reqBody) => {
  let updatedIsActive = { isActive: reqBody.isActive }

  return Course.findByIdAndUpdate(reqParams.courseId, updatedIsActive).then(
    (unArchiveResult, error) => (error ? false : true)
  )
}
