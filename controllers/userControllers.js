//User Model
const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Check if the email already exists
/*
  Steps:
    1) Use mangoose 'find' method to find a duplicate email
    2) Use the '.then()' method to send a response back to the frontend based on the result of the 'find()' method
*/
module.exports.checkEmailExists = (requestBody) => {
  // The result is sent back to the front end via the '.then()' method
  return User.find({ email: requestBody.email }).then((result) =>
    result.length > 0 ? true : false
  )
}

// User Registration
/*
  Steps:
  1) Create a new User object using the mongoose model and the information from the requestBody
  2) Make sure that the password is encrypted
  3) Save the new User to the database
*/

module.exports.registerUser = (requestBody) => {
  let newUser = new User({
    firstName: requestBody.firstName,
    lastName: requestBody.lastName,
    email: requestBody.email,
    mobileNumber: requestBody.mobileNumber,
    password: bcrypt.hashSync(requestBody.password, 12), // bcrypt implementation
  })
  return newUser.save().then((newUser, error) => (error ? false : true))
}

// Ask Ma'am Cee: Bakit hindi in-attach si .email sa userRoutes.js?

// User authentication
/*
	Steps:
	1) Check the database if the user email exists
	2) Compare the password provided in the login form with the password stored in the database
	3) Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (requestBody) => {
  return User.findOne({ email: requestBody.email }).then((result) => {
    if (result == null) return false
    else {
      const isPasswordCorrect = bcrypt.compareSync(
        requestBody.password,
        result.password
      )
      return isPasswordCorrect
        ? { access: auth.createAccessToken(result) }
        : false
    }
  })
}

// module.exports.getProfile = (requestBody) => {
//   return User.findOne({ id: requestBody.id }).then((result) => {
//     if (result == null) return false
//     else {
//       result.password = ''
//       return result
//     }
//   })
// }

module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = ''
    return result
  })
}
