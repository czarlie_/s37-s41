const jwt = require('jsonwebtoken')
// user defined string data that will be used to create our JSON web tokens
const secret = 'CourseBookingAPI'

//JSON Web Tokens
/*
  - JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
  - Information is kept secure through the use of the secret code
  - Only the system that knows the secret code that can decode the encrypted information

  - Imagine JWT as a gift wrapping service that secures the gift with a lock
  - Only the person who knows the secret code can open the lock
  - And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
  - This ensures that the data is secure from the sender to the receiver
*/

// Token creation
/*
  - Analogy
    Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  }
  // generates a JSON web token using the jwt's .sign() method
  // generate token using the data, and the secret with no additional options provided
  return jwt.sign(data, secret, {})
}

// Token Verification
/*
  Analogy:
  Received the gift and open the lock to verify if the sender is legitimate and the gift is not tampered with
*/

module.exports.verify = (request, response, next) => {
  // this token is retrieved from the request header > authorization
  // postman: Authorization > Bearer Token
  let token = request.headers.authorization

  if (typeof token !== 'undefined') {
    console.log(token)
    // result: 'Bearer fasdfagjasdfafadfaa' if no slice()
    // The token sent is a type of 'Bearer' token which when received contains the word 'Bearer' as a prefix to the string
    token = token.slice(7, token.length)
    console.log(`\n${token}`)
    // validate the token using the 'verify' method decrypting the token using the secret code (line 3: secret)
    return jwt.verify(token, secret, (error, data) =>
      // if the JWT is not valid : allows the app to proceed with the next middleware function in the route
      error ? response.send({ auth: 'failed' }) : next()
    )
  } else {
    return response.send({ auth: 'failed' })
  }
}

// Token Decryption
/*
  Analogy:
  Open the gift and get the content
*/

module.exports.decode = (token) => {
  if (typeof token !== 'undefined') {
    // Retrieves only the token and removes the 'Bearer' prefix
    token = token.slice(7, token.length)

    return jwt.verify(token, secret, (error, data) =>
      // The "decode" method is used to obtain the information from the JWT
      // The "{complete:true}" option allows us to return additional information from the JWT token
      // Returns an object with access to the "payload" property which contains user information stored when the token was generated
      // The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
      error ? null : jwt.decode(token, { complete: true }).payload
    )
  } else return null
}
