const express = require('express')
const mongoose = require('mongoose')
// allows our backend to be available to our frontend application
const cors = require('cors')

// allows access to routes defined within our application
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

const app = express()

// allows all resources to access our backend application
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

mongoose.set('strictQuery', true)
mongoose.connect(
  'mongodb+srv://admin:admin123@b248.ctuoffq.mongodb.net/booking-api?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
)

// connection to database
let bookingDatabase = mongoose.connection
bookingDatabase.on('error', console.error.bind(console, 'connection error'))
bookingDatabase.once('open', () =>
  // prettier-ignore
  console.log(
`|               Hello World! 👋🌏               |
|     We are connected to bookingDatabase 🥳💯  |
*===============================================*`
  )
)

// defines the /users string to be included for all user routes defined in the userRoutes.js
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)

app.listen(process.env.PORT || 4000, () => {
  // prettier-ignore
  console.log(
`*===============================================*
| API is now online via port │█║▌║▌║${process.env.PORT || 4000}║▌║▌║█│ |
*===============================================*`
  )
})
